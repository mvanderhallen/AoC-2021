{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)

  import Control.Monad.Identity (runIdentity, Identity)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  inputParser :: Parser PuzzleType
  inputParser = (,) <$> parsePlayer <*> parsePlayer
    where
      parsePlayer :: Parser Int
      parsePlayer = lexeme $ do
        symbol "Player"
        natural
        symbol "starting position: "
        fromIntegral <$> natural

  type PuzzleType = (Int,Int)
  data Player = P1 Int Int | P2 Int Int deriving (Eq, Show)

  cutOff :: Int -> Int
  cutOff 10 = 10
  cutOff x  = x `mod` 10

  add :: Player -> Int -> Player
  add (P1 pos score) y = P1 (cutOff $ pos + y) (score + (cutOff $ pos + y))
  add (P2 pos score) y = P2 (cutOff $ pos + y) (score + (cutOff $ pos + y))

  stop :: Player -> Bool
  stop (P1 _ score) = score >= 1000
  stop (P2 _ score) = score >= 1000


  withDieRolls :: (Player, Player) -> [Int] -> ((Player,Player),Int)
  withDieRolls (p1, p2) dieRolls = runIdentity $ go 0 (p1, p2) dieRolls
    where
      go :: Int -> (Player, Player) -> [Int] -> Identity ((Player, Player), Int)
      go x (curPlayer, nextPlayer) (v1:v2:v3:dieRolls') = do
        let roll = cutOff (v1 + v2 + v3)
        let curPlayer' = add curPlayer roll
        if stop curPlayer' then
          return ((curPlayer', nextPlayer), x+3)
        else
          go (x+3) (nextPlayer,curPlayer') dieRolls'

  score :: ((Player, Player), Int) -> Int
  score ((P1 _ _, P2 _ score), rolls) = score * rolls
  score ((P2 _ _, P1 _ score), rolls) = score * rolls

  test :: (Player, Player)
  test = (P1 4 0, P2 8 0)

  input :: (Player, Player)
  input = (P1 6 0, P2 9 0)

  assignment1 :: PuzzleType -> Int
  assignment1 (p1, p2) = score $ withDieRolls (P1 p1 0, P2 p2 0) $ concat $ repeat [1..100]

  data CurrPlayer = Player1 | Player2 deriving (Eq, Show)

  nbOfWinsAndLosses :: ((Int, Int), (Int, Int)) -> (Integer,Integer)
  nbOfWinsAndLosses = (oracle !!) . translate'
    where
      oracle = map (nbOfWinsAndLosses') [0..210210]

  nbOfWinsAndLosses' :: Int -> (Integer, Integer)
  nbOfWinsAndLosses' = nbOfWinsAndLosses'' . translate
  nbOfWinsAndLosses'' :: ((Int, Int), (Int,Int)) -> (Integer, Integer)
  nbOfWinsAndLosses'' st@((p1,s1), (p2,s2)) = foldl (\acc (r,s) -> vsum acc $ vmult s $ forDieRoll st r) (0,0) rolls
    where
      rolls = [(3,1),(4,3),(5,6),(6,7),(7,6),(8,3),(9,1)] -- map (\x -> (x!!0, length x)) $ group $ sort $ (sum <$> replicateM 3 [1,2,3])
  forDieRoll :: ((Int, Int), (Int, Int)) -> Int -> (Integer, Integer)
  forDieRoll state@((p1,s1), (p2,s2)) r =
    if (s1 + p1') >= 21 then
      (1,0)
    else
      vflip $ nbOfWinsAndLosses ((p2,s2),(p1',s1+p1'))
    where
      p1' = cutOff (p1+r)


  translate' :: ((Int, Int), (Int, Int)) -> Int
  translate' ((p1,s1),(p2,s2)) = (s1 * 10000 + p1' * 1000 + s2*10 + p2')
    where
      p1' = p1 `mod` 10
      p2' = p2 `mod` 10
  translate :: Int -> ((Int, Int), (Int, Int))
  translate x = ((restore $ part1 `mod` 10, part1 `div` 10), (restore $ part2 `mod` 10, part2 `div` 10))
    where
      part1 = x `div` 1000
      part2 = x `mod` 1000
      restore 0 = 10
      restore x = x

  allPos :: [((Int,Int), (Int, Int))]
  allPos = [((v1,v2),(v3,v4)) | v1<-[0..10], v3<-[0..10], v2<-[0..22], v4<-[0..22]]

  vsum :: (Integer, Integer) -> (Integer, Integer) -> (Integer,Integer)
  vsum (v1,v2) (v3, v4) = (v1+v3, v2 + v4)

  vmult :: Integer -> (Integer, Integer) -> (Integer, Integer)
  vmult s (v1,v2) = (s*v1,s*v2)

  vflip :: (Integer, Integer) -> (Integer, Integer)
  vflip (v1,v2) = (v2,v1)

  assignment2 :: PuzzleType -> Integer
  assignment2 (p1,p2) = uncurry max $ nbOfWinsAndLosses ((p1,0),(p2,0))

  table :: [(Int, Int, Integer)]
  table = [(p1,p2,uncurry max $ nbOfWinsAndLosses ((p1,0),(p2,0))) | p1<-[1..10], p2<-[1..10]]

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  -- main = print $ assignment2 undefined --print $ uncurry max $ nbOfWinsAndLosses' 6009
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
