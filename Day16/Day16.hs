{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, when)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  parseBits :: Parser PuzzleType
  parseBits = concatMap (toBits) <$> many1 parseHexa
    where
      parseHexa :: Parser Char
      parseHexa = oneOf "0123456789ABCDEF"
      toBits :: Char -> String -- [Int]
      toBits '0' = "0000"--[0,0,0,0]
      toBits '1' = "0001"--[0,0,0,1]
      toBits '2' = "0010"--[0,0,1,0]
      toBits '3' = "0011"--[0,0,1,1]
      toBits '4' = "0100"--[0,1,0,0]
      toBits '5' = "0101"--[0,1,0,1]
      toBits '6' = "0110"--[0,1,1,0]
      toBits '7' = "0111"--[0,1,1,1]
      toBits '8' = "1000"--[1,0,0,0]
      toBits '9' = "1001"--[1,0,0,1]
      toBits 'A' = "1010"--[1,0,1,0]
      toBits 'B' = "1011"--[1,0,1,1]
      toBits 'C' = "1100"--[1,1,0,0]
      toBits 'D' = "1101"--[1,1,0,1]
      toBits 'E' = "1110"--[1,1,1,0]
      toBits 'F' = "1111"--[1,1,1,1]

  toDecimal :: [Bool] -> Int
  toDecimal = foldl (\acc v -> acc * 2 + go v) 0
    where
      go True  = 1
      go False = 0

  inputParser :: Parser PuzzleType
  inputParser = parseBits

  type PuzzleType = String
  type Group  = (Bool, [Bool])
  data Packet = MkLitPacket { version :: Int, pType :: Int, groups :: [Group]}
              | MkOpPacket { version :: Int, pType :: Int, lType :: Bool, l :: Int, subpackets :: [Packet]}
              deriving (Show, Eq)

  parseBit :: Parser Bool
  parseBit =  return True <* symbol "1"
          <|> return False <* symbol "0"

  parsePacketVersion :: Parser Int
  parsePacketVersion = toDecimal <$> replicateM 3 parseBit

  parsePacketType :: Parser Int
  parsePacketType = toDecimal <$> replicateM 3 parseBit

  parseGroup :: Parser Group
  parseGroup = (,) <$> parseBit <*> replicateM 4 parseBit

  parseRelatedGroups :: Parser [Group]
  parseRelatedGroups = do
    (c, bits) <- parseGroup
    if c then
      (:) (c, bits) <$> parseRelatedGroups
    else
      return [(c,bits)]

  parsePadding :: Int -> Int -> Parser ()
  parsePadding offset groups = return () <* replicateM (4 - (offset + groups*5) `mod` 4) parseBit

  parseUnlimitedPadding :: Parser ()
  parseUnlimitedPadding = return () <* many (oneOf "0")

  parsePacket :: Bool -> Parser Packet
  parsePacket padded = do
    version <- parsePacketVersion
    pType   <- parsePacketType
    case pType of
      4 -> do
        groups <- parseRelatedGroups
        when padded $ parseUnlimitedPadding --parsePadding 6 (length groups)
        return $ MkLitPacket version pType groups
      _ -> do
        packet <- parseOperatorPacket version pType
        when padded $ parseUnlimitedPadding
        return packet

  parseOperatorPacket :: Int -> Int -> Parser Packet
  parseOperatorPacket version pType = do
    lType <- parseBit
    case lType of
      True -> do
        nbSubPackets <- toDecimal <$> replicateM 11 parseBit
        packets <- replicateM nbSubPackets (parsePacket False)
        return $ MkOpPacket version pType lType nbSubPackets packets
      False -> do
        subPacketLength <- toDecimal <$> replicateM 15 parseBit
        packetBits <- replicateM subPacketLength (oneOf "01")
        case runParser (many1 $ parsePacket False) () "Opator Packet Substream" packetBits of
          Left e -> error $ show e
          Right subPackets -> return $ MkOpPacket version pType lType subPacketLength subPackets

  sumVersions :: Packet -> Int
  sumVersions (MkLitPacket v _ _) = v
  sumVersions (MkOpPacket v _ _ _ ps) = v + (sum $ map sumVersions ps)

  assignment1 :: PuzzleType -> Int
  assignment1 input = case runParser (parsePacket True) () "Assignment 1" input of
    Left e  -> error $ show e
    Right p -> sumVersions p

  compute :: Packet -> Int
  compute (MkLitPacket _ _ gs) = toDecimal $ concatMap (snd) gs
  compute (MkOpPacket v p _ _ ps) = case p of
    0 -> sum subPacketValues
    1 -> product subPacketValues
    2 -> minimum subPacketValues
    3 -> maximum subPacketValues
    5 -> if subPacketValues!!0 > subPacketValues!!1 then 1 else 0
    6 -> if subPacketValues!!0 < subPacketValues!!1 then 1 else 0
    7 -> if subPacketValues!!0 == subPacketValues!!1 then 1 else 0
    where
      subPacketValues = map compute ps

  assignment2 :: PuzzleType -> Int
  assignment2 input = case runParser (parsePacket True) () "Assignment 1" input of
    Left e  -> error $ show e
    Right p -> compute p

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
