{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Control.Monad.Identity
  import Criterion.Main

  import Control.Monad.Primitive (PrimState, PrimMonad)
  import Data.Vector.Mutable (MVector)
  import qualified Data.Vector.Mutable as MV
  import Data.Vector (Vector)
  import qualified Data.Vector as V
  import Control.Monad.ST

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p



  type Row = Int
  type Col = Int
  data AOCLine = MkLine (Row,Col) (Row,Col) deriving (Eq, Show)

  data CellState = Clear | One | Two deriving (Eq, Show)

  increase :: CellState -> CellState
  increase Clear = One
  increase One = Two
  increase Two = Two

  parseLine :: Parser AOCLine
  parseLine = do
    start <- lexeme $ (,) <$> dec <* symbol "," <*> dec
    lexeme $ symbol "->"
    end <- lexeme $ (,) <$> dec <* symbol "," <*> dec
    return $ MkLine start end
    where
      dec = fromIntegral <$> decimal

  toCell :: Row -> Col -> Int
  toCell r c = r * 990 + c

  lineCells :: AOCLine -> [Int]
  lineCells (MkLine (c1,r1) (c2,r2))
    | r1 == r2 || c1 == c2 = [toCell r c |r<-[min r1 r2..max r1 r2], c <- [min c1 c2..max c1 c2]]
    | otherwise = []

  field :: PrimMonad m => m (MVector (PrimState m) CellState)
  field = MV.replicate (990*990) Clear

  set :: PrimMonad m => (MVector (PrimState m) CellState) -> Int -> m ()
  set v c = MV.modify v increase c

  handleLines :: [Int] -> Vector CellState
  handleLines cells = runIdentity $ do
    let finalField = runST $ processWithVector
    return $ V.filter (==Two) finalField
    where
      processWithVector :: PrimMonad m => m (Vector CellState)
      processWithVector = do
        initialField <- field
        mapM_ (set initialField) cells
        V.freeze initialField

  assignment1 :: FilePath -> IO Int
  assignment1 fp = do
    aoclines <- getAOCInput (many1 parseLine) fp
    return $ length $ handleLines (concatMap lineCells aoclines)

  lineCells2 :: AOCLine -> [Int]
  lineCells2 (MkLine (c1,r1) (c2,r2))
    | r1 == r2 || c1 == c2 = [toCell r c |r<-[min r1 r2..max r1 r2], c <- [min c1 c2..max c1 c2]]
    | otherwise = [toCell (offset delta r1 r2) (offset delta c1 c2) | delta <-[0..abs (r2-r1)]]
      where
        offset delta lim1 lim2
          | lim2 > lim1 = lim1 + delta
          | otherwise = lim1 - delta

  assignment2 :: FilePath -> IO Int
  assignment2 fp = do
    aoclines <- getAOCInput (many1 parseLine) fp
    return $ length $ handleLines (concatMap lineCells2 aoclines)




  main :: IO ()
  main = do
    args <- getArgs
    if (length $ filter (=="benchmark") args) == 1 then do
      withArgs (drop 1 args) $ criterion
    else do
      assignment1 "input.txt" >>= print
      assignment2 "input.txt" >>= print

  criterion :: IO ()
  criterion = defaultMain
    [
    bgroup "assignment1" $ [bench "" $ whnfIO (assignment1 "input.txt")],
    bgroup "assignment2" $ [bench "" $ whnfIO (assignment2 "input.txt")]
    ]
