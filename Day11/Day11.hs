{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import Control.Monad.State
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq
  import Data.IntMap.Strict (IntMap, empty, insert, (!), adjust, fromList)
  import qualified Data.IntMap.Strict as Map

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type OctoGrid = IntMap Int
  type Triggered = [Int]

  data PrintOctoGrid = PrintOctoGrid OctoGrid

  instance Show (PrintOctoGrid) where
    show (PrintOctoGrid grid) = unlines $ map line [0..9]
      where
        line :: Int -> String
        line y = concatMap (show . flip val y) [0..9]
        val :: Int -> Int -> Int
        val x y = grid ! (y*10+x)


  inputParser = toOctoGrid <$> parseGrid
    where
      parseGrid = many1 (lexeme $ line)
      line = many1 (read . return <$> oneOf "0123456789") :: Parser [Int]

  toOctoGrid :: [[Int]] -> IntMap Int
  toOctoGrid grid = foldr (\(x,y) -> insert (y*10+x) (grid!!y!!x)) empty [(x,y) |x <- [0..9], y <- [0..9]]

  neighbours :: Int -> [Int]
  neighbours i = do
    nb <- [y'*10+x' | x' <- [x-1..x+1], y' <- [y-1..y+1], x' >= 0, x' < 10, y' >= 0, y'<10]
    guard $ nb /= i
    return nb
    where
      (y,x) = i `quotRem` 10

  triggerAll :: Triggered
  triggerAll = [y*10+x | x<-[0..9], y<-[0..9]]

  step :: State OctoGrid Int
  step = sum <$> findFlashing triggerAll

  findFlashing :: Triggered -> State OctoGrid [Int]
  findFlashing [] = do
    limit
    return []
  findFlashing triggered = do
    flashed <- concat <$> mapM addOneTo triggered
    let trig = concatMap neighbours flashed
    ((length flashed) :) <$> findFlashing trig

  limit :: State OctoGrid ()
  limit = modify $ Map.map (\a -> if a >= 10 then 0 else a)

  addOneTo :: Int -> State OctoGrid [Int]
  addOneTo k = do
    val <- gets (flip (!) k)
    modify (adjust add k)
    if val == 9 then
      return [k]
    else
      return []

  add :: Int -> Int
  add v = v+1

  assignment1 :: OctoGrid -> Int
  assignment1 = sum . evalState (replicateM 100 step)

  stepUntil :: State OctoGrid Int
  stepUntil = do
    nbFlashed <- step
    if nbFlashed == 100 then
      return 1
    else
      (+1) <$> stepUntil

  assignment2 :: OctoGrid -> Int
  assignment2 = evalState stepUntil

  instance Puzzle.Puzzle OctoGrid where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @OctoGrid

  debugGrid :: OctoGrid
  debugGrid = fromList
    [(0,5),(1,4),(2,8),(3,3),(4,1),(5,4),(6,3),(7,2),(8,2),(9,3),(10,2),(11,7),(12,4),(13,5),(14,8),(15,5),(16,4),(17,7),(18,1),(19,1),(20,5),(21,2),(22,6),(23,4),(24,5),(25,5),(26,6),(27,1),(28,7),(29,3),(30,6),(31,1),(32,4),(33,1),(34,3),(35,3),(36,6),(37,1),(38,4),(39,6),(40,6),(41,3),(42,5),(43,7),(44,3),(45,8),(46,5),(47,4),(48,7),(49,8),(50,4),(51,1),(52,6),(53,7),(54,5),(55,2),(56,4),(57,6),(58,4),(59,5),(60,2),(61,1),(62,7),(63,6),(64,8),(65,4),(66,1),(67,7),(68,2),(69,1),(70,6),(71,8),(72,8),(73,2),(74,8),(75,8),(76,1),(77,1),(78,3),(79,4),(80,4),(81,8),(82,4),(83,6),(84,8),(85,4),(86,8),(87,5),(88,5),(89,4),(90,5),(91,2),(92,8),(93,3),(94,7),(95,5),(96,1),(97,5),(98,2),(99,6)]

  step1 :: OctoGrid
  step1 = fromList
    [(0,6),(1,5),(2,9),(3,4),(4,2),(5,5),(6,4),(7,3),(8,3),(9,4),(10,3),(11,8),(12,5),(13,6),(14,9),(15,6),(16,5),(17,8),(18,2),(19,2),(20,6),(21,3),(22,7),(23,5),(24,6),(25,6),(26,7),(27,2),(28,8),(29,4),(30,7),(31,2),(32,5),(33,2),(34,4),(35,4),(36,7),(37,2),(38,5),(39,7),(40,7),(41,4),(42,6),(43,8),(44,4),(45,9),(46,6),(47,5),(48,8),(49,9),(50,5),(51,2),(52,7),(53,8),(54,6),(55,3),(56,5),(57,7),(58,5),(59,6),(60,3),(61,2),(62,8),(63,7),(64,9),(65,5),(66,2),(67,8),(68,3),(69,2),(70,7),(71,9),(72,9),(73,3),(74,9),(75,9),(76,2),(77,2),(78,4),(79,5),(80,5),(81,9),(82,5),(83,7),(84,9),(85,5),(86,9),(87,6),(88,6),(89,5),(90,6),(91,3),(92,9),(93,4),(94,8),(95,6),(96,2),(97,6),(98,3),(99,7)]


  debugGrid2 :: OctoGrid
  debugGrid2 = fromList
    [(0,1),(1,1),(2,1),(3,1),(4,1),(5,0),(6,0),(7,0),(8,0),(9,0),(10,1),(11,9),(12,9),(13,9),(14,1),(15,0),(16,0),(17,0),(18,0),(19,0),(20,1),(21,9),(22,1),(23,9),(24,1),(25,0),(26,0),(27,0),(28,0),(29,0),(30,1),(31,9),(32,9),(33,9),(34,1),(35,0),(36,0),(37,0),(38,0),(39,0),(40,1),(41,1),(42,1),(43,1),(44,1),(45,0),(46,0),(47,0),(48,0),(49,0),(50,0),(51,0),(52,0),(53,0),(54,0),(55,0),(56,0),(57,0),(58,0),(59,0),(60,0),(61,0),(62,0),(63,0),(64,0),(65,0),(66,0),(67,0),(68,0),(69,0),(70,0),(71,0),(72,0),(73,0),(74,0),(75,0),(76,0),(77,0),(78,0),(79,0),(80,0),(81,0),(82,0),(83,0),(84,0),(85,0),(86,0),(87,0),(88,0),(89,0),(90,0),(91,0),(92,0),(93,0),(94,0),(95,0),(96,0),(97,0),(98,0),(99,0)]
