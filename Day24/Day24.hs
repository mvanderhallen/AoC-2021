{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Data.List

  import Control.Monad.State

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef { P.commentLine = "", P.commentStart = "", P.commentEnd = ""}
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Operation]

  type Register = Char
  type Value = Either Integer Register

  type Memory = ([Integer], (Integer, Integer, Integer, Integer))
  data Operation = Add Register Value
                 | Mul Register Value
                 | Div Register Value
                 | Mod Register Value
                 | Eql Register Value
                 | Inp Register deriving (Eq, Show)

  inputParser :: Parser PuzzleType
  inputParser = many1 (lexeme parseOperation)

  parseOperation :: Parser Operation
  parseOperation =  try (symbol "add") *> (Add <$> parseRegister <*> parseValue)
                <|> try (symbol "mul") *> (Mul <$> parseRegister <*> parseValue)
                <|> try (symbol "div") *> (Div <$> parseRegister <*> parseValue)
                <|> try (symbol "mod") *> (Mod <$> parseRegister <*> parseValue)
                <|> try (symbol "eql") *> (Eql <$> parseRegister <*> parseValue)
                <|> try (symbol "inp") *> (Inp <$> parseRegister)

  parseRegister :: Parser Register
  parseRegister = lexeme $ oneOf "wxyz"

  parseValue :: Parser Value
  parseValue = ((Left) <$> integer)
            <|> (Right <$> parseRegister)

  registers :: Memory -> (Integer, Integer, Integer, Integer)
  registers = snd

  eval :: Register -> State Memory Integer
  eval reg = gets (func reg . registers)
    where
      func 'w' (w,_,_,_) = w
      func 'x' (_,x,_,_) = x
      func 'y' (_,_,y,_) = y
      func 'z' (_,_,_,z) = z

  evalV :: Value -> State Memory Integer
  evalV (Left v)  = return v
  evalV (Right r) = eval r

  setReg :: Register -> Integer -> Memory -> Memory
  setReg r v (io, (w,x,y,z)) = case r of
    'w' -> (io, (v,x,y,z))
    'x' -> (io, (w,v,y,z))
    'y' -> (io, (w,x,v,z))
    'z' -> (io, (w,x,y,v))

  -- readInput :: Memory -> Integer
  -- readInput (io,_) = head io
  --
  -- dropInput :: Memory -> Memory
  -- dropInput ((_:io),r) = (io,r)

  moveIOToReg :: Register -> Memory -> Memory
  moveIOToReg r ((h:io),regs) = setReg r h (io,regs)

  perform :: Operation -> State Memory ()
  perform (Add r v) = do
    v' <- (+) <$> eval r <*> evalV v
    modify (setReg r v')
  perform (Mul r v) = do
    v' <- (*) <$> eval r <*> evalV v
    modify (setReg r v')
  perform (Div r v) = do
    v' <- div <$> eval r <*> evalV v
    modify (setReg r v')
  perform (Mod r v) = do
    v' <- mod <$> eval r <*> evalV v
    modify (setReg r v')
  perform (Eql r v) = do
    v' <- (==) <$> eval r <*> evalV v
    if v' then
      modify (setReg r 1)
    else
      modify (setReg r 0)
  perform (Inp r) = modify (moveIOToReg r)

  valid :: State Memory Bool
  valid = (==0) <$> eval 'z'

  calc :: [Operation] -> State Memory ()
  calc ops = mapM_ perform ops

  check :: [Operation] -> [Integer] -> Bool
  check ops io = evalState (calc ops *> valid) (io,(0,0,0,0))

  force :: PuzzleType -> [Int]
  force a = seq a $ reverse [0..13]

  assignment1 :: PuzzleType -> String
  assignment1 a = seq a $concatMap show $ snd $ head $ foldl (\acc v -> expand dup prefer v acc) [((0,0),[])] $ force a
    where
      prefer = \x y -> compare (snd y) (snd x)
      dup  = \x y -> fst x == fst y

  assignment2 :: PuzzleType -> String
  assignment2 a = concatMap show $ snd $ head $ foldl (\acc v -> expand dup prefer v acc) [((0,0),[])] $ force a
    where
      prefer = \x y -> compare (snd x) (snd y)
      dup  = \x y -> fst x == fst y

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType

  cipher :: [Integer] -> Integer
  cipher = foldl (cipherRound) 0 . zip magicNumbers

  cipherRound :: Integer -> ((Integer, Integer, Integer), Integer) -> Integer
  cipherRound prev ((a1,a2,a3), input) = ((prev `div` a1) * ((25 * weight)+1)) + ((input+a3) * weight)
    where
      weight = if (prev `mod` 26) + a2 == input then 0 else 1

  cipherRound' :: (Integer, Integer) -> (Integer, Integer, Integer) -> Integer -> (Integer, Integer)
  cipherRound' (d,m) (a1,a2,a3) i = newZ `quotRem` 26
    where
      newZ = cipherRound (d*26+m) ((a1,a2,a3),i)

  magicNumbers :: [(Integer,Integer,Integer)]
  magicNumbers = [(1,11,3), (1,14,7), (1,13,1), (26,-4,6),
                  (1,11,14), (1,10,7), (26,-4,9), (26,-12,9),
                  (1,10,6), (26,-11,4), (1,12,0), (26,-1,7),
                  (26,0,12), (26,-11,1)]

  possibilities :: (Integer,Integer,Integer) -> (Integer, Integer) -> [((Integer, Integer), Integer)]
  possibilities magic goal = [(prev,input)| input <- [9,8..1], prev <- searchSpace goal, cipherRound' prev magic input == goal]
    where
      searchSpace (d,r)= [(d',r')|d'<-[(d-1)*26..(d+1)*26]++[d-5..d+5] ++ [d`div`26-5 .. d`div`26+5], r' <-[0..25]]


  type Trace = ((Integer, Integer), [Integer])

  expand :: (Trace -> Trace -> Bool) -> (Trace -> Trace -> Ordering) -> Int -> [Trace] -> [Trace]
  expand dup prefer i current = nubBy (dup) $ sortBy (prefer) $ [(ng, inp' : inp)| (goal, inp) <- current, (ng, inp') <- possibilities (magicNumbers !! i) goal]
