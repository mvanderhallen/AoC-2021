{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, TypeFamilies  #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import qualified Puzzle as Puzzle

  import Data.Monoid
  import Data.Maybe
  import Data.List (partition, sort, tails)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [(Bool, Cuboid)]

  inputParser :: Parser PuzzleType
  inputParser = many1 parseCuboid
    where
      parseCuboid :: Parser (Bool, Cuboid)
      parseCuboid = lexeme $ do
        b <- parseBool
        xRange <- symbol "x=" *> parseRange <* symbol ","
        yRange <- symbol "y=" *> parseRange <* symbol ","
        zRange <- symbol "z=" *> parseRange
        return $ (b, MkCuboid xRange yRange zRange)
      parseRange :: Parser Range
      parseRange = (,) <$> (fromIntegral <$> integer) <* symbol ".." <*> (fromIntegral <$> integer)
      parseBool :: Parser Bool
      parseBool = try (return True <* symbol "on")
               <|> return False <* symbol "off"

  type Range = (Int, Int)
  data Cuboid = MkCuboid {xRange :: Range, yRange :: Range, zRange :: Range} deriving (Show, Eq, Ord)
  data Rectangle = MkRectangle {xx :: Range, yy :: Range} deriving (Show, Eq, Ord)
  type Point = (Int,Int,Int)

  class Container a where
    type Elem a
    equalSided :: Int -> Int -> a
    contains :: a -> Elem a -> Bool
    overlaps  :: a -> a -> Bool
    overlap :: a -> a -> Maybe a
    volume :: a -> Integer
    extract :: a -> a -> [a]

  instance Container Range where
    type Elem Range = Int
    equalSided x x' = (x,x')
    contains (x,y) z = z >= x && z <= y
    overlaps (x,y) (x',y') = not $ or [y < x', x > y']
    overlap (x,y) (x',y') =
      if overlaps (x,y) (x',y') then
        Just $ ((max x x'), (min y y'))
      else
        Nothing
    volume (x,y) = fromIntegral $ y-x+1
    extract r1@(x,y) r2 = case overlap r1 r2 of
      Just (x',y') -> filter ((>0) . volume) [(x,x'-1), (y'+1,y)]
      Nothing      -> [(x,y)]

  instance Container Rectangle where
    type Elem Rectangle = (Int, Int)
    equalSided x x' = MkRectangle (x,x') (x,x')
    contains (MkRectangle xx yy) (x,y) = contains xx x && contains yy y
    overlaps (MkRectangle xx yy) (MkRectangle xx' yy') = overlaps xx xx' && overlaps yy yy'
    overlap r1@(MkRectangle xx yy) r2@(MkRectangle xx' yy') =
      MkRectangle <$> (overlap xx xx') <*> (overlap yy yy')
    volume (MkRectangle xx yy) = volume xx * volume yy
    extract r1@(MkRectangle (x,x') (y,y')) r2 = case overlap r1 r2 of
      Nothing                    -> [r1]
      Just (MkRectangle (xx,xx') (yy,yy')) -> filter ((>0) . volume)
        [MkRectangle (x,x') (y,yy-1),
         MkRectangle (x,xx-1) (yy,yy'), MkRectangle (xx'+1,x') (yy,yy'),
         MkRectangle (x,x') (yy'+1,y')]

  instance Container Cuboid where
    type Elem Cuboid = Point
    equalSided x x' = MkCuboid (x,x') (x,x') (x,x')
    contains (MkCuboid {..}) (x,y,z) = contains xRange x && contains yRange y && contains zRange z
    overlaps (MkCuboid x y z) (MkCuboid x' y' z') = overlaps x x' && overlaps y y' && overlaps z z'
    overlap c1@(MkCuboid x y z) c2@(MkCuboid x' y' z') =
      MkCuboid <$> (overlap x x') <*> (overlap y y') <*> (overlap z z')
    volume (MkCuboid x y z) = volume x * volume y * volume z
    extract c1@(MkCuboid (x,x') (y,y') (z,z')) c2 = case overlap c1 c2 of
      Nothing -> [c1]
      Just (MkCuboid (xx,xx') (yy, yy') (zz, zz')) -> filter ((>0) . volume)

        [MkCuboid (x,x') (y,y') (z,zz-1),
        MkCuboid (x,xx-1) (y,yy-1) (zz,zz'), MkCuboid (xx,xx') (y,yy-1) (zz,zz'),  MkCuboid (xx'+1,x') (y,yy-1) (zz,zz'),
        MkCuboid (x,xx-1) (yy,yy')  (zz,zz'),                              MkCuboid (xx'+1,x') (yy,yy') (zz,zz'),
        MkCuboid (x,xx-1) (yy'+1,y') (zz,zz'), MkCuboid (xx,xx') (yy'+1,y') (zz,zz'), MkCuboid (xx'+1,x') (yy'+1,y') (zz,zz'),
        MkCuboid (x,x') (y,y') (zz'+1,z')]


  {- NAIVE SOLUTION FOR PART 1 -}
  act :: Point -> (Bool, Cuboid) -> First Bool
  act p (b,c) = if contains c p then First (Just b) else First Nothing


  isOn :: PuzzleType -> Point -> Bool
  isOn cuboids p = fromMaybe (False) $ getFirst $ mconcat $ map (act p) $ reverse cuboids

  assignment1' :: PuzzleType -> Int
  assignment1' input = length $ filter id $ [isOn relevant (x,y,z) | x<-[-50..50], y<-[-50..50], z<-[-50..50]]
    where
      relevant = filter (\(b,c) -> overlaps (MkCuboid (-50,50) (-50,50) (-50,50)) c) input
  {- END OF NAIVE SOLUTION FOR PART 1 -}

  volumeContribution :: forall a b . Container a => (Bool, a) -> [(b,a)] -> Integer
  volumeContribution (b,c) others
    | b = sum $ map volume $ uncovered c others
    | otherwise = 0

  uncovered :: forall a b . Container a => a -> [(b,a)] -> [a]
  uncovered c others = foldl (\acc otherCube -> concatMap (\c -> extract c otherCube) acc) [c] $ map snd others


  assignment1 :: Container a => [(Bool, a)] -> Integer
  assignment1 cubes = sum [volumeContribution c others | (c:others) <- tails cubes']
    where
      cubes' = [(b, fromJust $ overlap c window) | (b,c) <- cubes, overlaps c window]
      window = equalSided (-50) 50

  assignment2 :: Container a => [(Bool, a)] -> Integer
  assignment2 cubes = sum [volumeContribution c others | (c:others) <-tails cubes]

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType

  -- test = partition fst [(True,MkCuboid {xRange = (-5,47), yRange = (-31,22), zRange = (-19,33)}),(True,MkCuboid {xRange = (-44,5), yRange = (-27,21), zRange = (-14,35)}),(True,MkCuboid {xRange = (-49,-1), yRange = (-11,42), zRange = (-10,38)}),(True,MkCuboid {xRange = (-20,34), yRange = (-40,6), zRange = (-44,1)}),(False,MkCuboid {xRange = (26,39), yRange = (40,50), zRange = (-2,11)}),(True,MkCuboid {xRange = (-41,5), yRange = (-41,6), zRange = (-36,8)}),(False,MkCuboid {xRange = (-43,-33), yRange = (-45,-28), zRange = (7,25)}),(True,MkCuboid {xRange = (-33,15), yRange = (-32,19), zRange = (-34,11)}),(False,MkCuboid {xRange = (35,47), yRange = (-46,-34), zRange = (-11,5)}),(True,MkCuboid {xRange = (-14,36), yRange = (-6,44), zRange = (-16,29)}),(True,MkCuboid {xRange = (-57795,-6158), yRange = (29564,72030), zRange = (20435,90618)}),(True,MkCuboid {xRange = (36731,105352), yRange = (-21140,28532), zRange = (16094,90401)}),(True,MkCuboid {xRange = (30999,107136), yRange = (-53464,15513), zRange = (8553,71215)}),(True,MkCuboid {xRange = (13528,83982), yRange = (-99403,-27377), zRange = (-24141,23996)}),(True,MkCuboid {xRange = (-72682,-12347), yRange = (18159,111354), zRange = (7391,80950)}),(True,MkCuboid {xRange = (-1060,80757), yRange = (-65301,-20884), zRange = (-103788,-16709)}),(True,MkCuboid {xRange = (-83015,-9461), yRange = (-72160,-8347), zRange = (-81239,-26856)}),(True,MkCuboid {xRange = (-52752,22273), yRange = (-49450,9096), zRange = (54442,119054)}),(True,MkCuboid {xRange = (-29982,40483), yRange = (-108474,-28371), zRange = (-24328,38471)}),(True,MkCuboid {xRange = (-4958,62750), yRange = (40422,118853), zRange = (-7672,65583)}),(True,MkCuboid {xRange = (55694,108686), yRange = (-43367,46958), zRange = (-26781,48729)}),(True,MkCuboid {xRange = (-98497,-18186), yRange = (-63569,3412), zRange = (1232,88485)}),(True,MkCuboid {xRange = (-726,56291), yRange = (-62629,13224), zRange = (18033,85226)}),(True,MkCuboid {xRange = (-110886,-34664), yRange = (-81338,-8658), zRange = (8914,63723)}),(True,MkCuboid {xRange = (-55829,24974), yRange = (-16897,54165), zRange = (-121762,-28058)}),(True,MkCuboid {xRange = (-65152,-11147), yRange = (22489,91432), zRange = (-58782,1780)}),(True,MkCuboid {xRange = (-120100,-32970), yRange = (-46592,27473), zRange = (-11695,61039)}),(True,MkCuboid {xRange = (-18631,37533), yRange = (-124565,-50804), zRange = (-35667,28308)}),(True,MkCuboid {xRange = (-57817,18248), yRange = (49321,117703), zRange = (5745,55881)}),(True,MkCuboid {xRange = (14781,98692), yRange = (-1341,70827), zRange = (15753,70151)}),(True,MkCuboid {xRange = (-34419,55919), yRange = (-19626,40991), zRange = (39015,114138)}),(True,MkCuboid {xRange = (-60785,11593), yRange = (-56135,2999), zRange = (-95368,-26915)}),(True,MkCuboid {xRange = (-32178,58085), yRange = (17647,101866), zRange = (-91405,-8878)}),(True,MkCuboid {xRange = (-53655,12091), yRange = (50097,105568), zRange = (-75335,-4862)}),(True,MkCuboid {xRange = (-111166,-40997), yRange = (-71714,2688), zRange = (5609,50954)}),(True,MkCuboid {xRange = (-16602,70118), yRange = (-98693,-44401), zRange = (5197,76897)}),(True,MkCuboid {xRange = (16383,101554), yRange = (4615,83635), zRange = (-44907,18747)}),(False,MkCuboid {xRange = (-95822,-15171), yRange = (-19987,48940), zRange = (10804,104439)}),(True,MkCuboid {xRange = (-89813,-14614), yRange = (16069,88491), zRange = (-3297,45228)}),(True,MkCuboid {xRange = (41075,99376), yRange = (-20427,49978), zRange = (-52012,13762)}),(True,MkCuboid {xRange = (-21330,50085), yRange = (-17944,62733), zRange = (-112280,-30197)}),(True,MkCuboid {xRange = (-16478,35915), yRange = (36008,118594), zRange = (-7885,47086)}),(False,MkCuboid {xRange = (-98156,-27851), yRange = (-49952,43171), zRange = (-99005,-8456)}),(False,MkCuboid {xRange = (2032,69770), yRange = (-71013,4824), zRange = (7471,94418)}),(True,MkCuboid {xRange = (43670,120875), yRange = (-42068,12382), zRange = (-24787,38892)}),(False,MkCuboid {xRange = (37514,111226), yRange = (-45862,25743), zRange = (-16714,54663)}),(False,MkCuboid {xRange = (25699,97951), yRange = (-30668,59918), zRange = (-15349,69697)}),(False,MkCuboid {xRange = (-44271,17935), yRange = (-9516,60759), zRange = (49131,112598)}),(True,MkCuboid {xRange = (-61695,-5813), yRange = (40978,94975), zRange = (8655,80240)}),(False,MkCuboid {xRange = (-101086,-9439), yRange = (-7088,67543), zRange = (33935,83858)}),(False,MkCuboid {xRange = (18020,114017), yRange = (-48931,32606), zRange = (21474,89843)}),(False,MkCuboid {xRange = (-77139,10506), yRange = (-89994,-18797), zRange = (-80,59318)}),(False,MkCuboid {xRange = (8476,79288), yRange = (-75520,11602), zRange = (-96624,-24783)}),(True,MkCuboid {xRange = (-47488,-1262), yRange = (24338,100707), zRange = (16292,72967)}),(False,MkCuboid {xRange = (-84341,13987), yRange = (2429,92914), zRange = (-90671,-1318)}),(False,MkCuboid {xRange = (-37810,49457), yRange = (-71013,-7894), zRange = (-105357,-13188)}),(False,MkCuboid {xRange = (-27365,46395), yRange = (31009,98017), zRange = (15428,76570)}),(False,MkCuboid {xRange = (-70369,-16548), yRange = (22648,78696), zRange = (-1892,86821)}),(True,MkCuboid {xRange = (-53470,21291), yRange = (-120233,-33476), zRange = (-44150,38147)}),(False,MkCuboid {xRange = (-93533,-4276), yRange = (-16170,68771), zRange = (-104985,-24507)})]
