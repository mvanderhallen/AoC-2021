{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq
  import Data.HashMap.Strict (HashMap, empty, (!), fromList)
  import qualified Data.HashMap.Strict as Map
  import Data.Char (isUpper)
  import Data.List ((\\), intercalate)

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type Cave = String
  type CaveMap = HashMap Cave [Cave]

  parseCaves :: Parser CaveMap
  parseCaves = toCaveMap <$> many1 parseConnection
    where
      parseConnection :: Parser (Cave, Cave)
      parseConnection = lexeme $ (,) <$> (many1 letter) <* symbol "-" <*> (many1 letter)
      toCaveMap :: [(Cave, Cave)] -> CaveMap
      toCaveMap = foldr (\(f,t)-> add f t . add t f) empty
      add :: Cave -> Cave -> CaveMap -> CaveMap
      add f t m = Map.alter (\case
        Nothing -> Just [t]
        Just ls -> Just $ t:ls) f m

  isBig :: Cave -> Bool
  isBig (l:ls) = isUpper l -- more efficient than all (isUpper)... and semantically correct

  data Forbidden = MkForbidden { singleReuse :: Bool, caves :: [Cave]}

  getForbidden :: Forbidden -> [Cave]
  getForbidden (MkForbidden False _)     = ["start"]
  getForbidden (MkForbidden True caves)  = caves

  addToForbidden :: Forbidden -> Cave -> Forbidden
  addToForbidden fb@(MkForbidden r caves) cave =
    if isBig cave then
      fb
    else if (cave `elem` caves) then
      MkForbidden True (caves)
    else
      MkForbidden r (cave:caves)

  paths :: Bool -> CaveMap -> [[Cave]]
  paths noReuse cm = fromJust $ doStep (MkForbidden noReuse ["start"]) "start"
    where
      doStep :: Forbidden -> Cave -> Maybe [[Cave]]
      doStep fb "end" = Just [["end"]]
      doStep fb f = case go of
        [] -> Nothing
        ls -> Just ls
        where
          go = do
            ns <- nextStep cm fb f
            let fb' = addToForbidden fb ns
            case doStep fb' ns of
              Nothing -> []
              Just ls -> map (f:) ls

  nextStep :: CaveMap -> Forbidden -> Cave -> [Cave]
  nextStep cm fb c = (cm ! c) \\ (getForbidden fb)

  assignment1 :: CaveMap -> Int
  assignment1 = length . paths True

  assignment2 :: CaveMap -> Int
  assignment2 = length . paths False

  instance Puzzle.Puzzle CaveMap where
    parseInput = parseCaves
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @CaveMap

  printPaths :: [[Cave]] -> IO ()
  printPaths = putStr . unlines . map (intercalate ",")
