{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}

  import qualified Puzzle as Puzzle

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import Data.List
  import Data.Maybe (catMaybes, fromMaybe, fromJust, isJust)
  import Data.Monoid

  import Debug.Trace

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser $ haskellDef { P.commentLine = "", P.commentStart = "", P.commentEnd = ""}
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [Scanner]
  type Position = (Int, Int, Int)
  data Scanner = MkScanner { scannerId :: Integer, beaconPositions :: [Position]} deriving (Show, Eq)

  mkScanner i = MkScanner i . sort

  instance Num Position where
    (+) (x,y,z) (x',y',z') = (x+x', y+y', z+z')
    (*) (x,y,z) (x',y',z') = (x*x', y*y', z*z')
    (-) (x,y,z) (x',y',z') = (x-x', y-y', z-z')
    abs (x,y,z) = (abs x, abs y, abs z)
    fromInteger = undefined
    signum = undefined

  inputParser :: Parser PuzzleType
  inputParser = many1 parseScanner

  parseScanner :: Parser Scanner
  parseScanner = lexeme $ do
    symbol "---"
    symbol "scanner"
    scannerId <- natural
    symbol "---"
    mkScanner scannerId <$> many1 parsePosition

  parsePosition :: Parser Position
  parsePosition = try (lexeme $ do
    x <- fromIntegral <$> integer <* symbol ","
    y <- fromIntegral <$> integer <* symbol ","
    z <- fromIntegral <$> integer
    return (x,y,z))

  match :: Int -> Scanner -> Scanner -> Maybe Scanner
  match n s1 s2 = toMaybe $ filter (go s1) $ rotate s2
    where
      go :: Scanner -> Scanner -> Bool
      go (MkScanner _ ps1) (MkScanner _ ps2) = (>= threshold) $ length $ [p2-p1|p1<-ps1, p2 <- ps1, p1<p2] `intersection` [p2-p1 | p1 <- ps2, p2 <- ps2, p1 < p2]
      threshold :: Int
      threshold = n*(n-1) `div` 2
      intersection = intersect
      toMaybe :: [a] -> Maybe a
      toMaybe []     = Nothing
      toMaybe (x:xs) = Just x

  offset :: Int -> Scanner -> Scanner -> Maybe Position
  offset n (MkScanner _ ps1) (MkScanner _ ps2) = getFirst $ mconcat $ [tryAsMatcher (p1-p2) |p1<-ps1, p2 <- ps2]
    where
      tryAsMatcher :: Position -> First Position
      tryAsMatcher offs =
        if n <= length ((map (\x -> offs + x) ps2) `intersect` ps1) then
          First $ Just offs
        else
          First $ Nothing

  join :: Scanner -> Scanner -> Maybe (Position, Scanner)
  join s1@(MkScanner i ps) s2@(MkScanner _ ps2) = do
    s' <- match 12 s1 s2
    let offs = fromJust $ offset 12 s1 s'
    return (offs, mkScanner i $ nub $ ps ++ map (\x -> offs + x) (beaconPositions s'))

  joinAll :: [Scanner] -> ([(Integer, Position)], Scanner)
  joinAll [s] = ([], s)
  joinAll (s:ss) = ((scannerId sAdded, p) : positions, result)
    where
      (positions, result) = joinAll (sExp:(delete sAdded ss))
      Just (sAdded, (p, sExp)) = getFirst $ mconcat $ map First [(s',) <$> join s s' | s' <- ss] :: Maybe (Scanner, (Position, Scanner))

  -- joinOrder :: [Scanner] -> [(Int,Int)]
  -- joinOrder scanners =
  --
  --     connections = [(x,y) | x<-[0..l-1], y<-[0..l-1], x/=y, isJust $ match 12 (scanners!!x) (scanners!!y)]
  --     l = length scanners

  rotate :: Scanner -> [Scanner]
  rotate (MkScanner scannerId pos) = map (MkScanner scannerId) $ map (\f -> map f pos) swaps
    where
        swaps = [\(x,y,z) -> (x,y,z),
          \(x,y,z) -> (x,-z,y),
          \(x,y,z) -> (z,y,-x),
          \(x,y,z) -> (x,-y,-z),
          \(x,y,z) -> (y,-z,-x),
          \(x,y,z) -> (z,x,y),
          \(x,y,z) -> (-x,y,-z),
          \(x,y,z) -> (x,z,-y),
          \(x,y,z) -> (-z,-y,-x),
          \(x,y,z) -> (y,x,-z),
          \(x,y,z) -> (-x,-z,-y),
          \(x,y,z) -> (z,-y,x),
          \(x,y,z) -> (-x,z,y),
          \(x,y,z) -> (-z,y,x),
          \(x,y,z) -> (-y,z,-x),
          \(x,y,z) -> (-z,x,-y),
          \(x,y,z) -> (-x,-y,z),
          \(x,y,z) -> (y,z,x),
          \(x,y,z) -> (-y,-z,x),
          \(x,y,z) -> (z,-x,-y),
          \(x,y,z) -> (-z,-x,y),
          \(x,y,z) -> (-y,x,z),
          \(x,y,z) -> (y,-x,z),
          \(x,y,z) -> (-y,-x,-z)]

  assignment1 :: PuzzleType -> Int
  assignment1 input = length $ beaconPositions $ snd $ joinAll input

  assignment2 :: PuzzleType -> Int
  assignment2 input = manhattan positions
    where
      positions = sort $ map snd $ fst $ joinAll input

  manhattan :: [Position] -> Int
  manhattan pos = last $ sort $ [size (p2-p1) | p1<-pos, p2<-pos]
  size (x,y,z) = abs x + abs y + abs z

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
