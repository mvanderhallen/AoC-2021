{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM)
  import Criterion.Main

  import Data.IntMap.Strict (IntMap)
  import qualified Data.IntMap.Strict as IntMap
  import Data.Vector ((!), accum, Vector, fromList)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p


  parser = (,) <$> parseNumbers <*> many (parseBoard)

  parseNumbers :: Parser [Int]
  parseNumbers = lexeme $ sepBy (fromIntegral <$> decimal) (char ',')

  parseBoard :: Parser BingoBoard
  parseBoard = lexeme $ do
    board <- Control.Monad.replicateM 5 (Control.Monad.replicateM 5 (fromIntegral <$> (lexeme $ decimal))) :: Parser [[Int]]
    let imap = IntMap.fromList [(key, (r, c)) | r <- [0..4], c <- [0..4], let key = (board!!r)!!c]
    return $ BingoBoard imap init init
      where
        init = fromList [0,0,0,0,0]


  data BingoBoard = BingoBoard (IntMap (Int,Int)) (Vector Int) (Vector Int) | Bingo Int deriving Show

  mark :: Int -> BingoBoard -> BingoBoard
  mark _ bb@(Bingo {}) = bb
  mark val bb@(BingoBoard nbs rows cols) =
    case nbs IntMap.!? val of
      Nothing    -> bb
      Just (r,c) ->
        if win then
          Bingo score
        else
          BingoBoard (IntMap.delete val nbs) (updateBB rows r) (updateBB cols c)
            where
              win = (rows ! r == 4) || (cols ! c == 4)
              updateBB :: Vector Int -> Int -> Vector Int
              updateBB vector key = accum (+) vector [(key,1)]
              score = val*(-val + IntMap.foldrWithKey (\k _ acc -> acc+k) 0 nbs)

  assignment1 fp = do
    (nbs, boards) <- getAOCInput parser fp
    return $ filter bingo $ head $ dropWhile (not . any bingo) $ scanl (\acc val -> map (mark val) acc) boards nbs

  assignment2 fp = do
    (nbs, boards) <- getAOCInput parser fp
    let losingBoard =  head $ head $
            dropWhile (\bs -> length bs /= 1) $
            scanl (\acc val -> filter (not . bingo) $ map (mark val) acc) boards nbs
    return $ foldl (\acc val -> mark val acc) losingBoard nbs

  bingo :: BingoBoard -> Bool
  bingo BingoBoard{} = False
  bingo Bingo{} = True

  main :: IO ()
  main = do
    args <- getArgs
    if (length $ filter (=="benchmark") args) == 1 then do
      withArgs (drop 1 args) $ criterion
    else do
      assignment1 "input.txt" >>= print
      assignment2 "input.txt" >>= print

  criterion :: IO ()
  criterion = defaultMain
    [
    bgroup "assignment1" $ [bench "" $ whnfIO (assignment1 "input.txt")],
    bgroup "assignment2" $ [bench "" $ whnfIO (assignment2 "input.txt")]
    ]
