{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Control.Monad.Identity
  import Criterion.Main

  import Control.Monad.Primitive (PrimState, PrimMonad)
  import Data.Vector.Mutable (MVector)
  import qualified Data.Vector.Mutable as MV
  import Data.Vector (Vector)
  import qualified Data.Vector as V
  import Control.Monad.ST

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace


  input :: [Int]
  input = [3,5,2,5,4,3,2,2,3,5,2,3,2,2,2,2,3,5,3,5,5,2,2,3,4,2,3,5,5,3,3,5,2,4,5,4,3,5,3,2,5,4,1,1,1,5,1,4,1,4,3,5,2,3,2,2,2,5,2,1,2,2,2,2,3,4,5,2,5,4,1,3,1,5,5,5,3,5,3,1,5,4,2,5,3,3,5,5,5,3,2,2,1,1,3,2,1,2,2,4,3,4,1,3,4,1,2,2,4,1,3,1,4,3,3,1,2,3,1,3,4,1,1,2,5,1,2,1,2,4,1,3,2,1,1,2,4,3,5,1,3,2,1,3,2,3,4,5,5,4,1,3,4,1,2,3,5,2,3,5,2,1,1,5,5,4,4,4,5,3,3,2,5,4,4,1,5,1,5,5,5,2,2,1,2,4,5,1,2,1,4,5,4,2,4,3,2,5,2,2,1,4,3,5,4,2,1,1,5,1,4,5,1,2,5,5,1,4,1,1,4,5,2,5,3,1,4,5,2,1,3,1,3,3,5,5,1,4,1,3,2,2,3,5,4,3,2,5,1,1,1,2,2,5,3,4,2,1,3,2,5,3,2,2,3,5,2,1,4,5,4,4,5,5,3,3,5,4,5,5,4,3,5,3,5,3,1,3,2,2,1,4,4,5,2,2,4,2,1,4]
  -- input :: IO [String]
  -- input = lines <$> readFile "input.txt"
  --
  -- getAOCInput :: Parser a -> FilePath -> IO a
  -- getAOCInput p fp = do
  --   input <- readFile fp
  --   case parse p fp input of
  --     Left err -> do
  --       print err
  --       error "Parse error"
  --     Right p  -> return p

  data Sea = MkSea [Int] deriving Show

  toSea :: [Int] -> Sea
  toSea sea = MkSea $ map (\v->length $ filter (==v) sea) [0..8]

  evolve2 :: Sea -> Sea
  evolve2 (MkSea sea) = MkSea $ onethroughsix ++ [zero+seven, eight, zero]
    where
      zero = head sea
      onethroughsix = take 6 $ drop 1 $ sea
      seven = head $ drop 7 sea
      eight = head $ drop 8 sea

  assignment' :: Sea -> Int -> Int
  assignment' sea days = count $ foldr (\_ -> evolve2) sea [1..days]
    where
      count (MkSea currSea) = sum currSea

  -- Very quick win for the first star
  -- evolve :: Int -> [Int]
  -- evolve 0 = [6,8]
  -- evolve x = return $ x-1
  --
  -- computeLengthAfterDays input days = length $ foldr (\_ acc -> concatMap evolve acc) input [1..days]





  -- assignment1 :: FilePath -> IO Int
  -- assignment1 fp = do
  --   aoclines <- getAOCInput (many1 parseLine) fp
  --   return $ length $ handleLines (concatMap lineCells aoclines)
  --
  -- lineCells2 :: AOCLine -> [Int]
  -- lineCells2 (MkLine (c1,r1) (c2,r2))
  --   | r1 == r2 || c1 == c2 = [toCell r c |r<-[min r1 r2..max r1 r2], c <- [min c1 c2..max c1 c2]]
  --   | otherwise = [toCell (offset delta r1 r2) (offset delta c1 c2) | delta <-[0..abs (r2-r1)]]
  --     where
  --       offset delta lim1 lim2
  --         | lim2 > lim1 = lim1 + delta
  --         | otherwise = lim1 - delta
  --
  -- assignment2 :: FilePath -> IO Int
  -- assignment2 fp = do
  --   aoclines <- getAOCInput (many1 parseLine) fp
  --   return $ length $ handleLines (concatMap lineCells2 aoclines)




  -- main :: IO ()
  -- main = do
  --   args <- getArgs
  --   if (length $ filter (=="benchmark") args) == 1 then do
  --     withArgs (drop 1 args) $ criterion
  --   else do
  --     assignment1 "input.txt" >>= print
  --     assignment2 "input.txt" >>= print
  --
  -- criterion :: IO ()
  -- criterion = defaultMain
  --   [
  --   bgroup "assignment1" $ [bench "" $ whnfIO (assignment1 "input.txt")],
  --   bgroup "assignment2" $ [bench "" $ whnfIO (assignment2 "input.txt")]
  --   ]

  main :: IO ()
  main = defaultMain
    [
    bgroup "assignment1" $ [bench "" $ whnf (assignment' (toSea input)) 80],
    bgroup "assignment2" $ [bench "" $ whnf (assignment' (toSea input)) 256]
    ]
