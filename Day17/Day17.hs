{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, guard)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)

  import Debug.Trace

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = Target
  type Target = ((Int,Int), (Int,Int))
  type Velocity = Int

  validXVelocity :: Target -> [Int]
  validXVelocity ((xmin, xmax), _)= do
    vx <- [0..xmax]
    guard $ ((vx*(vx+1)) `div` 2) >= xmin
    if ((vx*(vx+1)) `div` 2) < xmax then
      return vx
    else if any (within (xmin,xmax)) $ scanr (+) 0 [1..vx] then
      return vx
    else
      []

  within (mini, maxi) v = v >= mini && v <= maxi

  validYVelocity :: Target -> Int -> [(Int, Int, Int)]
  validYVelocity t@((xmin, xmax), (ymin, ymax)) vx = do
    vy <- [ymin..(-ymin)]
    let px = xpos t vx
    let py = ypos t vy
    if any (\(x,y) -> within (xmin,xmax) x && within (ymin, ymax) y) $ zip px py then
      return (vx,vy, maximum py)
    else
      []

  xpos :: Target -> Velocity -> [Int]
  xpos ((xmin, xmax), _) vx = takeWhile (<= xmax) $ scanl (+) 0 $ (reverse [1..vx]) ++ repeat 0

  ypos :: Target -> Velocity -> [Int]
  ypos (_, (ymin, _)) vy= do
    let vys = vy : map (subtract 1) vys
    takeWhile (>= ymin) $ scanl (+) 0 vys


  inputParser :: Parser PuzzleType
  inputParser = do
    string "target area: x="
    return ((0,0),(0,0))
    xbounds <- (,) <$> (fromIntegral <$> integer) <* symbol ".." <*> (fromIntegral <$> integer)
    symbol ","
    symbol "y="
    ybounds <- (,) <$> (fromIntegral <$> integer) <* symbol ".." <*> (fromIntegral <$> integer)
    return (xbounds, ybounds)


  assignment1 :: PuzzleType -> Int
  assignment1 target = maximum $ map (\(_,_,h) -> h) $ concatMap (validYVelocity target) $ validXVelocity target

  assignment2 :: PuzzleType -> Int
  assignment2 target = length $ concatMap (validYVelocity target) $ validXVelocity target

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType

  test :: PuzzleType
  test = ((10,20),(0,10))
