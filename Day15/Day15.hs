{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, sortOn)
  import Data.Maybe (fromJust, catMaybes)

  import Data.HashMap.Strict (HashMap, empty, fromList, unionWith, lookup)
  import Control.Monad.State

  import Prelude hiding (lookup)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = [[Risk]]
  type Risk = Int
  type RiskMap = HashMap (Int,Int) Risk

  inputParser :: Parser PuzzleType
  inputParser = do many1 $ lexeme $ many1 parseRisk
    where
      parseRisk = (read . return) <$> oneOf "0123456789"

  neighbours :: Int -> Int -> (Int, Int) -> [(Int, Int)]
  neighbours w h (x,y) = do
    (dx,dy) <- [(1,0), (-1,0), (0,1), (0,-1)]
    guard $ x+dx >= 0
    guard $ x+dx < w
    guard $ y+dy >= 0
    guard $ y+dy < h
    return (x+dx, y+dy)

  dijkstra :: PuzzleType -> State RiskMap Risk
  dijkstra grid = go [((0,0),0)]
    where
      width = length $ grid !! 0
      height = length grid
      nbs :: (Int, Int) -> [(Int, Int)]
      nbs = neighbours width height
      go :: [((Int, Int), Int)] -> State RiskMap Risk
      go (((x,y), accumRisk) : pos) = do
        let neighbours = nbs (x,y)
        if (width-1, height-1) `elem` neighbours
          then return $ accumRisk + (grid !! (height-1) !! (width-1))
        else do
          riskMap <- get
          let nbsRisk = catMaybes $ map (calcRisk riskMap) neighbours :: [((Int,Int), Int)]
          modify (unionWith (\v1 -> const v1) (fromList nbsRisk))
          go (mergeOn snd (sortOn snd nbsRisk) pos)
          where
            calcRisk :: RiskMap -> (Int,Int) -> Maybe ((Int,Int), Int)
            calcRisk rm (x',y') = case lookup (x',y') rm of
              Nothing -> Just ((x',y'),accumRisk + grid !! y' !! x')
              Just i  -> let newRisk = accumRisk + grid !! y' !! x' in
                if newRisk < i then
                  Just ((x',y'), newRisk)
                else
                  Nothing

  mergeOn :: Ord b => (a->b) -> [a] -> [a] -> [a]
  mergeOn _ [] [] = []
  mergeOn _ [] bs = bs
  mergeOn _ as [] = as
  mergeOn f (a:as) (b:bs)
    | compare (f a) (f b) == GT = b : mergeOn f (a:as) bs
    | otherwise = a : mergeOn f as (b:bs)

  assignment1 :: PuzzleType -> Int
  assignment1 grid = evalState (dijkstra grid) empty

  printGrid :: PuzzleType -> String
  printGrid = unlines . map (concatMap show)

  bigMap :: PuzzleType -> Int -> PuzzleType
  bigMap grid n = concatMap (conc . constructRow) [0..n]
    where
      augmentWith :: Int -> [[Int]]
      augmentWith x = map (map (\v -> rollover (v + x))) grid
      rollover x
        | x > 9 = rollover (x-9)
        | otherwise = x
      constructRow row = map (augmentWith) [row..row+n] :: [[[Int]]]
      conc :: [[[Int]]] -> [[Int]]
      conc g = map (\line -> concatMap (\n -> g!!n!!line) [0..(length g - 1)]) [0..(length grid)-1]

  assignment2 :: PuzzleType -> Int
  assignment2 grid = assignment1 $ bigMap grid 4

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
