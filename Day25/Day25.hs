{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import Data.Set (Set)
  import qualified Data.Set as S

  import qualified Puzzle as Puzzle

  import Control.Monad.State

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace


  type PuzzleType = [[GridElem]]
  data GridElem = RightE | DownE | EmptyE deriving (Eq, Ord)
  data GridMap = GridMap { w :: Int, h :: Int, right :: Set Int, down :: Set Int} deriving (Eq, Ord, Show) --, rightMove :: Int -> Int, downMove :: Int -> Int}

  -- instance Eq GridMap where
  --   (==) (GridMap w h r d _ _)  (GridMap w' h' r' d' _ _) = w == w' && h == h' && r == r' && d == d'
  -- instance Show GridMap where
  --   show (GridMap w h r d _ _) = show $ unwords $ ["GridMap", show w, show h, show r, show d]

  instance Show GridElem where
    show RightE = ">"
    show DownE = "v"
    show EmptyE = "."

  inputParser :: Parser PuzzleType
  inputParser = many1 parseLine
    where
      parseLine = lexeme $ many1 parseGridElem
      parseGridElem :: Parser GridElem
      parseGridElem =  try (oneOf ">") *> return RightE
                   <|> try (oneOf "v") *> return DownE
                   <|> try (oneOf ".") *> return EmptyE

  toGridMap :: PuzzleType -> GridMap
  toGridMap grid = GridMap w h l r -- right down
    where
      (l,r) = foldl (update) (S.empty, S.empty) $ [(x,y) |x<-[0..w-1], y<-[0..h-1]]
      update (l,r) p@(x,y) = case grid !! y !! x of
        RightE -> (S.insert (trans p) l, r)
        DownE  -> (l,S.insert (trans p) r)
        EmptyE -> (l,r)
      trans (x,y) = y * w + x
      trans' v = v `quotRem` w
      right = trans . (\(r,c) -> (r, (c+1)`mod` w)) . trans'
      down = trans . (\(r,c) -> ((r+1)`mod`h, c)) . trans'
      w = length $ grid !! 0
      h = length $ grid

  -- class Indexable a where
  --   type Elem a
  --   type Pos a
  --   at :: a -> Pos a -> Elem a
  --
  -- instance Indexable PuzzleType where
  --   type Elem PuzzleType = Bool
  --   type Pos =

  moveDown :: GridMap -> Int -> Int
  moveDown (GridMap w h _ _) p = trans $ ((r+1) `mod` h, c)
    where
      (r,c) = p `quotRem` w
      trans (r,c) = r*w + c

  moveRight :: GridMap -> Int -> Int
  moveRight (GridMap w h _ _) p = trans $ (r, (c+1) `mod` w)
    where
      (r,c) = p `quotRem` w
      trans (r,c) = r*w + c


  step :: State GridMap Bool
  step = do
    gm@(GridMap w h r d) <- get
    let r' = S.map (\a -> if moveRight gm a `S.member` r || moveRight gm a `S.member` d then a else moveRight gm a) r
    let d' = S.map (\a -> if moveDown gm a `S.member` r' || moveDown gm a `S.member` d then a else moveDown gm a) d
    put (GridMap w h r' d')
    return (r' == r && d' == d)

  stepUntil :: State GridMap Int
  stepUntil = go 0
    where
      go :: Int -> State GridMap Int
      go i = do
        stop <- step
        if stop then
          return $ i+1
        else
          go $ i+1


  assignment1 :: PuzzleType -> Int
  assignment1 = evalState stepUntil . toGridMap

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . const ""

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
