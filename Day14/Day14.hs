{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Control.Monad (zipWithM)
  import Data.List (sort, partition, (\\), nub, delete, foldl', group, maximumBy, minimumBy)
  import Data.Maybe (fromJust)
  import Data.HashMap.Strict (HashMap, empty, alter, unionWith, toList, fromList)
  import Control.Monad.State
  import Data.Hashable (Hashable)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = PolymerCooking

  type Polymer = String
  type Cookbook = [(String, Char)]

  data PolymerCooking = PolymerCooking Polymer Cookbook deriving (Show, Eq)

  inputParser :: Parser PuzzleType
  inputParser = PolymerCooking <$> parsePolymer <*> (many1 parseCookbook)
    where
      parseElement = oneOf "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      parsePolymer = lexeme $ many1 $ parseElement
      parseCookbook = lexeme $ (,) <$> parsePolymer <* symbol "->" <*> parseElement

  cook :: Cookbook -> Polymer -> Polymer
  cook cookbook polymer = concat $ zipWith rule polymer (drop 1 polymer ++ " ")
    where
      rule a ' ' = [a]
      rule a b = [a,fromJust $ lookup ([a,b]) cookbook]

  analyzeFreq :: Polymer -> [Int]
  analyzeFreq = map (length) . group . sort

  assignment1 :: PuzzleType -> Integer
  assignment1 puzzle = cookNRounds puzzle 10
  {-go $ analyzeFreq $ foldr (const $ cook cookbook) polymer [0..9]
    where
      go ls = (maximum ls) - (minimum ls)-}

  type CountMap = HashMap Char Integer
  type PolymerBinds = HashMap Polymer Integer

  analyzeFreq' :: (Hashable c, Eq c) => [c] -> HashMap c Integer
  analyzeFreq' = foldl' (flip (alter go)) empty
    where
      go Nothing = Just 1
      go (Just x) = Just (x+1)

  pressureCooker :: Cookbook -> PolymerBinds -> State CountMap PolymerBinds
  pressureCooker cookbook polymerDec = foldM rule empty $ toList polymerDec
    where
      go :: Integer -> Maybe Integer -> Maybe Integer
      go m Nothing = Just m
      go m (Just x) = Just (x+m)
      rule :: PolymerBinds -> (Polymer, Integer) -> State CountMap PolymerBinds
      rule hm ([a,b], mult) = do
        let insertion = fromJust $ lookup [a,b] cookbook
        modify (alter (go mult) insertion)
        let hm' = alter (go mult) [a,insertion] $ alter (go mult) [insertion,b] hm
        return hm'

  deconstruct :: Polymer -> PolymerBinds
  deconstruct polymer = analyzeFreq' $ zipWith (\a b -> [a,b]) polymer (drop 1 polymer)

  cookNRounds ::PuzzleType -> Integer -> Integer
  cookNRounds (PolymerCooking polymer cookbook) n = result $ go n
    where
      result :: HashMap a Integer -> Integer
      result hm =  maxi - mini
        where
          maxi = maximum $ map snd $ toList hm
          mini = minimum $ map snd $ toList hm
      initialFreq   :: CountMap
      initialFreq    = (analyzeFreq' polymer)
      initialPolymer :: PolymerBinds
      initialPolymer = deconstruct polymer
      go :: Integer -> CountMap
      go n = flip execState initialFreq $ foldM (\polymerDec _ -> pressureCooker cookbook polymerDec) initialPolymer [0..n-1]

  assignment2 :: PuzzleType -> Integer
  assignment2 puzzle = cookNRounds puzzle 40

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
