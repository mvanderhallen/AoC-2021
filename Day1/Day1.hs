module Day1 where
  input :: IO [String]
  input = lines <$> readFile "input.txt"

  assignment1 :: IO ()
  assignment1 = do
    integers <- (map read) <$> input :: IO [Int]
    print $ length $ filter id $ zipWith (>) (drop 1 integers) integers
    return ()

  assignment2 :: IO ()
  assignment2 = do
    integers <- (map read) <$> input :: IO [Int]
    let windowed = zipWith3 (\x y z -> x + y + z) integers (drop 1 integers) (drop 2 integers)
    print $ length $ filter id $ zipWith (>) (drop 1 windowed) windowed
    return ()

  main :: IO ()
  main = do
    assignment1
    assignment2
