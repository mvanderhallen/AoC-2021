{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}

  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = (Lookup, Grid)

  type Lookup = [Bool]
  data Grid = MkGrid { grid :: [[Bool]], infinite:: Bool} deriving Show

  inputParser :: Parser PuzzleType
  inputParser = (,) <$> parseRow <*> (MkGrid <$> many1 parseRow <*> return False)
    where
      parseRow = lexeme $ many1 parsePixel
      parsePixel = return True <* oneOf "#"
                <|> return False <* oneOf "."

  readBinary :: [Bool] -> Int
  readBinary = foldl (\acc v -> acc * 2 + val v) 0
    where
      val True  = 1
      val False = 0

  enhance :: PuzzleType -> PuzzleType
  enhance (lu, g@(MkGrid grid inf)) = (lu, MkGrid [[calculate x y| x <- [-1..width]]| y <- [-1..height]] infinite')
    where
      width = length $ grid !! 0
      height = length grid
      infinite' = lu !! (readBinary $ replicate 9 inf)
      calculate x y = lu !! readBinary [evalGrid g x' y' | y' <- [y-1..y+1], x' <- [x-1..x+1]]

  evalGrid :: Grid -> Int -> Int -> Bool
  evalGrid (MkGrid grid inf) x y
    | x < 0 || y < 0 = inf
    | x >= length (grid!!0) || y >= length grid = inf
    | otherwise = grid !! y !! x

  printGrid :: Grid -> IO ()
  printGrid g@(MkGrid grid _) = putStrLn $ unlines $ [[charOf $ evalGrid g x y|x <- [-1..length (grid!!0)]] | y <- [-1..length grid]]
    where
      charOf True = '#'
      charOf False = '.'

  assignment1 :: PuzzleType -> Int
  assignment1 puzzle = countLights $ snd $ enhance $ enhance puzzle
    where
      countLights (MkGrid grid inf) = length $ filter id $ concat grid

  assignment2 :: PuzzleType -> Int
  assignment2 puzzle = countLights $ snd $ foldl (\puzzle _ -> enhance puzzle) puzzle $ replicate 50 0
    where
      countLights (MkGrid grid inf) = length $ filter id $ concat grid

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
