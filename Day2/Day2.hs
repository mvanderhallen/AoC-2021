{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Day2 where
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type Displacement = (Int, Int)

  parseDisplacement :: Parser Displacement
  parseDisplacement =
    ((,0) . fromIntegral) <$> (symbol "forward" *> decimal)
    <|>
    ((,0) . negate . fromIntegral) <$> (symbol "backward" *> decimal)
    <|>
    ((0,) . fromIntegral) <$> (symbol "down" *> decimal)
    <|>
    ((0,) . negate . fromIntegral) <$> (symbol "up" *> decimal)


  assignment1 :: IO ()
  assignment1 = do
    displacements <- getAOCInput (many $ lexeme parseDisplacement) "input.txt"
    print $ uncurry (*)  $ foldl (\(x,y) (dx,dy) -> (x+dx,y + dy)) (0,0) displacements

  assignment2 :: IO ()
  assignment2 = do
    displacements <- getAOCInput (many $ lexeme parseDisplacement) "input.txt"
    print $ (\(x,y,_) -> x*y)  $ foldl (\(x,y,aim) (dx,da) -> (x+dx,y + dx * aim, aim+da)) (0,0,0) displacements

  main :: IO ()
  main = do
    assignment1
    assignment2
