{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  data Display = MkDisplay { uniques :: [String], readings :: [String]} deriving (Show)

  mkDisplay un rs = MkDisplay (map sort un) (map sort rs)

  parseDisplay :: Parser Display
  parseDisplay = lexeme $ do
    un <- uniques
    lexeme $ symbol "|"
    op <- output
    return $ MkDisplay un op
    where
      output = replicateM 4 ddigit :: Parser [String]
      uniques = replicateM 10 ddigit :: Parser [String]
      ddigit = sort <$> (lexeme $ many1 wire) :: Parser String
      wire = C.oneOf   "abcdefg" :: Parser Char

  countOneFourSevenEight :: Display -> Int
  countOneFourSevenEight (MkDisplay _ readings) = ones + fours + sevens + eights
    where
      ones = length $ filter (\x -> length x == 2) readings
      fours = length $ filter (\x -> length x == 4) readings
      sevens = length $ filter (\x -> length x == 3) readings
      eights = length $ filter (\x -> length x == 7) readings

  assignment1 :: [Display] -> Int
  assignment1 displays = sum $ map countOneFourSevenEight displays

  data Wire = MkWire { canonical :: Char, actual :: Char} deriving Show

  deduceWires :: Display -> String -> Int
  deduceWires (MkDisplay uniques _) =
    --([wireA,wireB,wireD, wireE], [one,four,seven,eight,three, two, five, zero, six, nine])
    \s -> fromJust $ lookup (sort s) $ zip [zero,one,two,three,four,five,six,seven,eight,nine] [0..9]
      where
        zero = head $ filter (\x -> (not $ containsWire wireD x) && length x == 6) uniques :: String
        one = head $ filter (\x -> length x == 2) uniques :: String
        two = head $ filter (/=three) $ filter (\x -> (not $ containsWire wireB x) && length x == 5) uniques :: String
        three = head $ filter (\x -> isSubsetOf one x && (length x == 5)) uniques :: String
        four = head $ filter (\x -> length x == 4) uniques :: String
        five = head $ filter (\x -> (containsWire wireB x) && length x == 5) uniques :: String
        six = head $ filter(\x -> (containsWire wireD x) && (containsWire wireE x) && length x == 6) uniques :: String
        seven = head $ filter (\x -> length x == 3) uniques :: String
        eight = head $ filter (\x -> length x == 7) uniques :: String
        nine = head $ filter (\x -> (containsWire wireD x) && (not $ containsWire wireE x) && length x == 6) uniques :: String
        wireA = MkWire 'a' (head $ seven \\ one)
        wireB = MkWire 'b' (head $ four \\ three)
        wireD = MkWire 'd' (head $ removeWire wireB $ four \\ one)
        wireE = MkWire 'e' (head $ two \\ three)

      -- wireE = MkWire 'e' ()

  isSubsetOf :: String -> String -> Bool
  isSubsetOf s1 s2 = (sort s2) == (sort $ nub $ s1 ++ s2)

  containsWire :: Wire -> String -> Bool
  containsWire (MkWire _ a) = elem a

  removeWire :: Wire -> [Char] -> String
  removeWire (MkWire _ a) = delete a

  output :: Display -> Int
  output display = foldl (\acc v-> acc*10 + identify v) 0 $ readings display
    where
      identify = deduceWires display

  assignment2 :: [Display] -> Int
  assignment2 displays = sum $ map output displays

  instance Puzzle.Puzzle [Display] where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @[Display]
  -- main = do
  --   args <- getArgs
  --   let fp = head args
  --   if (length $ filter (=="benchmark") args) == 1 then do
  --     withArgs (drop 2 args) $ criterion fp
  --   else do
  --     input <- getAOCInput inputParser fp
  --     print $ assignment1 input
  --     print $ assignment2 input


  inputParser :: Parser [Display]
  inputParser = many parseDisplay

  criterion :: FilePath -> IO ()
  criterion fp = do
    input <- getAOCInput inputParser fp
    print input
    -- input `deepseq` return ()
    defaultMain
      [
      bgroup "Gather input" $ [bench fp $ whnfIO (getAOCInput inputParser "input.txt")],
      bgroup "Assignment #1" $ [bench fp $ whnf assignment1 input],
      bgroup "Assignment #2" $ [bench fp $ whnf assignment2 input]
      ]
