{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, foldl1)
  import Data.Maybe (fromJust)
  import Debug.Trace

  -- import Data.Vector hiding ((++))

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  inputParser :: Parser PuzzleType
  inputParser = many1 parseSnail
    where
      parseSnail :: Parser Snail
      parseSnail = lexeme $ many1 parseSnailElem
      parseSnailElem :: Parser SnailElem
      parseSnailElem = try (return Start <* oneOf "[" <* parseComma)
                    <|> try (return End <* oneOf "]" <* parseComma)
                    <|> try ((Val . fromIntegral) <$> integer <* parseComma)
      parseComma = many $ (symbol ",")

  type PuzzleType = [Snail]
  data SnailElem = Start | End | Val Int deriving (Eq)--, Show)
  type Snail = [SnailElem]

  data ShowSnail = ShowSnail Snail deriving Eq

  instance Show SnailElem where
    show Start = "["
    show End = "]"
    show (Val x) = show x

  instance {-# OVERLAPPING #-} Show Snail where
    show snail = unwords $ map show snail

  instance Show (ShowSnail) where
    show (ShowSnail snail) = unwords $ map show snail

  add :: Snail -> Snail -> Snail
  add s1 s2 = reduce rawAddition
    where
      rawAddition = Start : (s1 ++ s2 ++ [End])

  explode :: Snail -> (Bool, Snail)
  explode = flip go (0,[])
    where
      go :: Snail -> (Int, Snail) -> (Bool, Snail)
      go [] (_,acc) = (False, reverse acc)
      go (Start:snail) (4,acc) = (True, snail')
        where
          (Val v1 : Val v2 : End : remainder) = snail
          snail' = (reverse (changeFirstVal (+v1) acc)) ++ Val 0 : (changeFirstVal (+v2) remainder)
      go (Start:snail) (x, acc) = go snail (x+1, Start : acc)
      go (End  :snail) (x, acc) = go snail (x-1, End : acc)
      go (Val v:snail) (x, acc) = go snail (x, Val v : acc)

  changeFirstVal :: (Int -> Int) -> Snail -> Snail
  changeFirstVal _ [] = []
  changeFirstVal f (Start:s) = Start : changeFirstVal f s
  changeFirstVal f (End:s) = End : changeFirstVal f s
  changeFirstVal f (Val x:s) = Val (f x) : s

  split :: Snail -> (Bool, Snail)
  split [] = (False, [])
  split (Start : snail) = (Start:) <$> split snail
  split (End : snail) = (End:) <$> split snail
  split (Val x : snail)
    | x < 10 = ((Val x):) <$> split snail
    | x >= 10 = (True, Start : Val v1 : Val v2 : End : snail)
      where
        v1 = floor $ (fromIntegral x) / 2
        v2 = ceiling $ (fromIntegral x) / 2

  reduce :: Snail -> Snail
  reduce s =
    if bExplode then
      reduce sExplode
    else if bSplit then
      reduce sSplit
    else
      s
    where
      (bExplode, sExplode) = explode s
      (bSplit, sSplit) = split s

  magnitude :: Snail -> Int
  magnitude = go [] [1]
    where
      go :: [Int] -> [Int] -> Snail -> Int
      go (magnitude:vs) _ [] = magnitude
      go stack (m:mult) (Val x : s) = go (m*x:stack) mult s
      go stack mult (Start : s) = go stack (3:2:mult) s
      go stack (m:mult) (End : s)   = go (m*(v1+v2) : stack') mult s
        where
          (v1 : v2 : stack') = stack

  printSnail :: Snail -> IO ()
  printSnail = print . ShowSnail

  assignment1 :: [Snail] -> Int
  assignment1 = magnitude . foldl1 add

  assignment2 :: PuzzleType -> Int
  assignment2 snails = maximum $ [magnitude $ add s1 s2| s1<-snails, s2 <- snails]

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
