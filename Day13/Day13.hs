{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete, foldl', sortOn)
  import Data.Maybe (fromJust)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = PuzzleInput

  data PuzzleInput = MkPuzzleInput [Point] [Fold] deriving (Show, Eq)

  data Fold = Yis Int | Xis Int deriving (Show, Eq)
  type Point = (Int, Int)

  inputParser :: Parser PuzzleType
  inputParser = MkPuzzleInput <$> many1 parsePoint <*> many1 parseFold
    where
      parsePoint :: Parser (Int, Int)
      parsePoint = (,) <$> (fromIntegral <$> natural) <* symbol "," <*> (fromIntegral <$> natural)
      parseFold :: Parser Fold
      parseFold = do
        symbol "fold along"
        axis <- oneOf "xy"
        symbol "="
        val <- fromIntegral <$> natural
        return (if axis == 'x' then Xis val else Yis val)

  doFold :: [Point] -> Fold -> [Point]
  doFold ps (Yis val) = nub $ map go ps
    where
      go (x,y) =
        if y > val then
          (x, (val - (y `mod` (val))) `mod` val)
        else
          (x,y)
  doFold ps (Xis val) = nub $ map go ps
    where
      go (x,y) =
        if x > val then
          ((val - (x `mod` (val))) `mod` val, y)
        else
          (x,y)


  assignment1 :: PuzzleType -> Int
  assignment1 (MkPuzzleInput ps folds) = length $ doFold ps (head folds)

  assignment2 :: PuzzleType -> String
  assignment2 (MkPuzzleInput ps folds) = showPoints $ foldl' doFold ps folds

  showPoints :: [Point] -> String
  showPoints ps = unlines $ [[c | x<-[0..maxX], let c = if ((x,y) `elem` ps) then '#' else '.'] | y<-[0..maxY]]
    where
      maxX :: Int
      maxX = maximum $ map fst ps
      maxY = maximum $ map snd ps

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType

  debug = MkPuzzleInput [(6,10),(0,14),(9,10),(0,3),(10,4),(4,11),(6,0),(6,12),(4,1),(0,13),(10,12),(3,4),(3,0),(8,4),(1,10),(2,14),(8,10),(9,0)] [Yis 7, Xis 5]
