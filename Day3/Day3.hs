{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM)
  import Criterion.Main

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  input :: IO [String]
  input = lines <$> readFile "input.txt"

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  parseBinaryNumber :: Parser [Int]
  parseBinaryNumber = lexeme $ many1 pChar
    where
      pChar = (char '1' *> return 1) <|> (char '0' *> return 0)

  assignment1 :: FilePath -> IO Int
  assignment1 fp = do
    binaryNumbers <- getAOCInput (many parseBinaryNumber) fp :: IO [[Int]]
    let halfSize = length binaryNumbers `div` 2
    let counts = map ( > halfSize) $ (foldl (\acc x -> zipWith (+) acc x) (repeat 0) binaryNumbers)
    return $ (gamma counts) * (epsilon counts)

  gamma :: [Bool] -> Int
  gamma = foldl (\acc b -> if b then acc * 2 + 1 else acc*2) 0

  epsilon :: [Bool] -> Int
  epsilon = foldl (\acc b -> if not b then acc * 2 + 1 else acc*2) 0

  data Trie = EmptyTrie | Trie (Int, Trie) (Int, Trie) deriving Show

  insert :: [Int] -> Trie -> Trie
  insert [0] (EmptyTrie) = Trie (1, EmptyTrie) (0, EmptyTrie)
  insert [1] (EmptyTrie) = Trie (0, EmptyTrie) (1, EmptyTrie)
  insert [0] (Trie (l, lt) (r,rt)) = Trie (l+1, lt) (r,rt)
  insert [1] (Trie (l, lt) (r,rt)) = Trie (l, lt) (r+1,rt)
  insert (v:xs) (EmptyTrie)
    | v == 0 = Trie (1, insert xs EmptyTrie) (0, EmptyTrie)
    | v == 1 = Trie (0, EmptyTrie) (1, insert xs EmptyTrie)
  insert (v:xs) (Trie (l,lt) (r,rt))
    | v == 0 = Trie (l+1, insert xs lt) (r, rt)
    | v == 1 = Trie (l, lt) (r+1, insert xs rt)


  buildTrie = foldr (insert) EmptyTrie

  descendTrie :: Bool -> Trie -> [Int]
  descendTrie _ (EmptyTrie) = []
  descendTrie b (Trie (0,_) (r, rt)) = 1 : descendTrie b rt
  descendTrie b (Trie (l,lt) (0, _)) = 0 : descendTrie b lt
  descendTrie b (Trie (l, lt) (r,rt)) =
    case compare l r of
      EQ -> if b then 1 : descendTrie b rt else 0 : descendTrie b lt
      LT -> if b then 1 : descendTrie b rt else 0 : descendTrie b lt
      GT -> if b then 0 : descendTrie b lt else 1 : descendTrie b rt

  left :: Trie -> Trie
  left (Trie (_,lt) _) = lt

  right :: Trie -> Trie
  right (Trie _ (_,rt)) = rt

  viewNode :: Trie -> (Int, Int)
  viewNode (Trie (l,_) (r,_)) = (l,r)
  viewNode (EmptyTrie) = (0,0)


  getValues :: Trie -> [[Int]]
  getValues (EmptyTrie) = [[]]
  getValues (Trie (l, lt) (r, rt)) = left ++ right
    where
      left = if l > 0 then  map (0:) (getValues lt) else []
      right = if r > 0 then map (1:) (getValues rt) else []

  assignment2 :: FilePath -> IO Int
  assignment2 fp = do
    binaryNumbers <- getAOCInput (many parseBinaryNumber) fp :: IO [[Int]]
    let binaryTrie = buildTrie binaryNumbers
    let oxy = descendTrie True binaryTrie
    let scrub = descendTrie False binaryTrie
    return $ (toDecimal oxy) * (toDecimal scrub)

  toDecimal :: [Int] -> Int
  toDecimal = foldl (\acc v-> acc*2+v) (0)

  main :: IO ()
  main = do
    args <- getArgs
    if (length $ filter (=="benchmark") args) == 1 then do
      withArgs (drop 1 args) $ criterion
    else do
      assignment1 "input.txt" >>= print
      assignment2 "input.txt" >>= print

  criterion :: IO ()
  criterion = defaultMain
    [
    bgroup "assignment1" $ [bench "" $ whnfIO (assignment1 "input.txt")],
    bgroup "assignment2" $ [bench "" $ whnfIO (assignment2 "input.txt")]
    ]
