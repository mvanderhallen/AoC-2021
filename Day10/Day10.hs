{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import Data.List (sort)

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  getAOCInput :: Parser a -> FilePath -> IO a
  getAOCInput p fp = do
    input <- readFile fp
    case parse p fp input of
      Left err -> do
        print err
        error "Parse error"
      Right p  -> return p

  type Navigation = String

  data ParseResult = MkIncomplete [Char] | MkCorrupted {expected :: Char, found :: Char} deriving (Show, Eq)

  inputParser :: Parser [Navigation]
  inputParser = many1 $ lexeme $ many1 $ oneOf "<[({})]>"

  starter :: Char -> Char
  starter ')' = '('
  starter '}' = '{'
  starter '>' = '<'
  starter ']' = '['

  closer :: Char -> Char
  closer '(' = ')'
  closer '{' = '}'
  closer '<' = '>'
  closer '[' = ']'

  parseCheck :: Navigation -> ParseResult
  parseCheck nav = go [] nav
    where
      go stack [] = MkIncomplete stack
      go stack (n:nav')
        | n `elem` "<[{(" = go (n:stack) nav'
        | n `elem` ">]})" = case stack of
            []      -> error "Empty stack when closing"
            (sh:st) -> if starter n == sh
              then
                go st nav'
              else
                MkCorrupted (closer sh) n

  scoreParseResult :: ParseResult -> Int
  scoreParseResult (MkIncomplete{})  = 0
  scoreParseResult (MkCorrupted _ f) = table f
    where
      table ')' = 3
      table ']' = 57
      table '}' = 1197
      table '>' = 25137

  assignment1 :: [Navigation] -> Int
  assignment1 = sum . map (scoreParseResult . parseCheck)

  type AutoCompletion = String

  autoComplete :: Navigation -> AutoCompletion
  autoComplete = go . parseCheck
    where
      go (MkCorrupted{}) = ""
      go (MkIncomplete openPairs) = map closer openPairs

  scoreAutoCompletion :: AutoCompletion -> Int
  scoreAutoCompletion = foldl (\acc v -> acc * 5 + table v) 0
    where
      table ')' = 1
      table ']' = 2
      table '}' = 3
      table '>' = 4

  middle :: [Int] -> Int
  middle ls = ls !! l
    where
      l = length ls `div` 2

  assignment2 :: [Navigation] -> Int
  assignment2 = middle . sort . filter (/= 0) . map (scoreAutoCompletion . autoComplete)

  instance Puzzle.Puzzle [Navigation] where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @[Navigation]

  interactive :: FilePath -> IO ()
  interactive fp = withArgs [fp] main
