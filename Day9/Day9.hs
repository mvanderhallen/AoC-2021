{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
  import System.Environment
  import Text.Parsec
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)
  import Control.Monad (replicateM, foldM, guard)
  import Criterion.Main
  import Control.DeepSeq

  import Data.List (sort, partition, (\\), nub, delete)
  import Data.Maybe (fromJust)
  import Data.Set (Set)
  import qualified Data.Set as S

  import qualified Puzzle as Puzzle

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  parseGrid :: Parser Grid
  parseGrid = many1 (lexeme $ line)
    where
      line = many1 (read . return <$> oneOf "0123456789") :: Parser [Int]

  type Grid = [[Int]]

  testGrid :: Grid
  testGrid = [[2,1,9,9,9,4,3,2,1,0],[3,9,8,7,8,9,4,9,2,1],[9,8,5,6,7,8,9,8,9,2],[8,7,6,7,8,9,6,7,8,9],[9,8,9,9,9,6,5,6,7,8]]

  neighbours width height grid x y = do
    guard $ x >= 0
    guard $ y >= 0
    guard $ x < width
    guard $ y < height
    [(x',y')|dx<-[-1,0,1], dy<-[-1,0,1], (abs dx) + (abs dy) == 1, let x' = x+dx, let y' = y + dy, x' >= 0, y'>=0, x' < width, y' < height]

  eval :: Grid -> (Int,Int) -> Int
  eval grid (x,y) = grid !! y !! x

  lowpoints :: Grid -> [(Int,Int)]
  lowpoints grid = do
    x <- [0..width-1]
    y <- [0..height-1]
    let n = map (eval grid) $ neighbours width height grid x y
    guard $ not $ any (<= grid!!y!!x) n
    return (x,y)
    --filter (\(x,y) -> not $ any (<= grid!!y!!x) $ neighbours width height grid x y) [(x,y) | x <- [0..width-1], y <- [0..height-1]]
    where
      height = length grid
      width = length $ grid !! 0

  evalRisk grid x y = 1 + grid !! y !! x

  assignment1 :: Grid -> Int
  assignment1 grid = sum $ map (uncurry $ (evalRisk grid)) $ lowpoints grid

  data Basin = MkBasin (Int,Int) Int deriving (Eq,Show)

  instance Ord Basin where
    compare (MkBasin _ s1) (MkBasin _ s2) = compare s1 s2

  size :: Basin -> Int
  size (MkBasin _ s) = s

  findBasin :: Grid -> (Int, Int) -> Basin
  findBasin grid (x,y) = MkBasin (x,y) $ length $ floodfill grid S.empty [(x,y)]
    where
      floodfill :: Grid -> Set (Int,Int) -> [(Int, Int)] -> [(Int, Int)]
      floodfill grid vis [] = []
      floodfill grid vis ((x,y):tovis) =
        if (grid!!y!!x == 9) then
          floodfill grid (S.insert (x,y) vis) tovis
        else do
          let ns = neighbours width height grid x y
          let nsToAdd = filter (flip S.notMember vis) ns
          (x,y) : floodfill grid (S.insert (x,y) vis) (nub $ nsToAdd ++ (tovis))
        where
          height = length grid
          width = length $ grid !! 0

  assignment2 :: Grid -> Int
  assignment2 grid = product $ map size $ take 3 $ reverse $ sort $ map (findBasin grid) $ lowpoints grid



  instance Puzzle.Puzzle Grid where
    parseInput = parseGrid
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @Grid
