{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}


  import Text.Parsec hiding (State)
  import qualified Text.Parsec.Token as P
  import qualified Text.Parsec.Char as C
  import Text.Parsec.Language (haskellDef)

  import qualified Puzzle as Puzzle

  import Data.List
  import Control.Monad
  import Data.Maybe
  import qualified Data.Set as S

  type Parser a = Parsec String () a
  lexer = P.makeTokenParser haskellDef
  lexeme = P.lexeme lexer
  symbol = P.symbol lexer
  integer = P.integer lexer
  natural = P.natural lexer
  decimal = P.decimal lexer
  whiteSpace = P.whiteSpace lexer
  whitespace = whiteSpace

  type PuzzleType = ()

  inputParser :: Parser PuzzleType
  inputParser = symbol "" *> return ()

  type Pos = (Int, Int)

  data AmphiState = AmphiState { amber :: [(Int, Int)], bronze :: [(Int, Int)], copper :: [(Int, Int)], desert :: [(Int, Int)]} deriving (Show, Eq, Ord)

  mkState a b c d = AmphiState (sort a) (sort b) (sort c) (sort d)

  data Amphipod = Amber | Bronze | Copper | Desert deriving (Eq, Show)

  data Move = MkMove { typ :: Amphipod, from :: Pos, to :: Pos } deriving (Eq, Show)

  mkMove = MkMove

  steps :: Move -> [Pos]
  steps (MkMove _ (r,c) (r',c'))
    | c' == c = [(r'',c)|r''<-[min r r' .. max r r']]
    | otherwise = filter (/= (r,c)) $ nub $ concat [toHallway, inHallway, fromHallway]
      where
        toHallway = if r > 0 then reverse [(r'',c) | r'' <- [0..r]] else []
        inHallway = if c /= c' then [(0,c'') | c'' <- [min c c'..max c c']] else []
        fromHallway = if r' > 0 then [(r'', c') | r''<-[0.. r']] else []

  energyOf :: Move -> Int
  energyOf m@(MkMove typ from to) = (mult typ) * (length $ steps m)
    where
      mult Amber = 1
      mult Bronze = 10
      mult Copper = 100
      mult Desert = 1000

  room :: Pos -> Maybe [Pos]
  room (0,_) = Nothing
  room (r,c) = Just [(1,c), (2,c)]

  occupied :: AmphiState -> [Pos]
  occupied s = amber s ++ bronze s ++ copper s ++ desert s

  occupiedBy :: AmphiState -> Amphipod -> [Pos]
  occupiedBy s Amber = amber s
  occupiedBy s Bronze = bronze s
  occupiedBy s Copper = copper s
  occupiedBy s Desert = desert s

  occupiedExcept :: AmphiState -> Amphipod -> [Pos]
  occupiedExcept s Amber = bronze s ++ copper s ++ desert s
  occupiedExcept s Bronze = amber s ++ copper s ++ desert s
  occupiedExcept s Copper = amber s ++ bronze s ++ desert s
  occupiedExcept s Desert = amber s ++ bronze s ++ copper s

  update :: AmphiState -> Move -> AmphiState
  update s (MkMove Amber p1 p2) = s { amber = sort $ p2 : delete p1 (amber s)}
  update s (MkMove Bronze p1 p2) = s { bronze = sort $ p2 : delete p1 (bronze s)}
  update s (MkMove Copper p1 p2) = s { copper = sort $ p2 : delete p1 (copper s)}
  update s (MkMove Desert p1 p2) = s { desert = sort $ p2 : delete p1 (desert s)}

  stop :: AmphiState -> Bool
  stop s = and [all ((==2) . snd) $ amber s,
                all ((==4) . snd) $ bronze s,
                all ((==6) . snd) $ copper s,
                all ((==8) . snd) $ desert s]

  moves :: Int -> AmphiState -> [Move]
  moves d s = do
    typeToMove <- [Amber, Bronze, Copper, Desert]
    from <- occupiedBy s typeToMove
    to <- movesFor d typeToMove from
    let move = mkMove typeToMove from to
    guard $ validate s move
    return move
    where
      movesFor :: Int -> Amphipod -> Pos -> [Pos]
      movesFor d t (r,c) = case (targetRoom t==c, roomState) of
        (True, True)  -> [(depth,c) | depth <-[r..d]]
        (True, False) -> hallway ++ room
        (False, True)  -> hallway ++ room
        (False, False) -> hallway
        where
          hallway = [(0,0),(0,1), (0,3), (0,5), (0,7), (0,9),(0,10)]
          room = [(depth, targetRoom t) | depth <- [1..d]]
          roomState = all ((==t) . fst) $ podsInRoom s (targetRoom t)

  validate :: AmphiState -> Move -> Bool
  validate s m@(MkMove typ from to) = and [from /= to, noHallwayStartEnd, noCollision', roomCondition]
    where
      noCollision' :: Bool
      noCollision' = null $ (steps m) `intersect` (occupied s)
      noHallwayStartEnd :: Bool
      noHallwayStartEnd = ((fst from) /= 0) || ((fst to) /= 0)
      roomCondition :: Bool
      roomCondition = (fst to == 0) || (and $ map (not . flip elem (occupiedExcept s typ)) $ fromMaybe [] $ room to)

  withEnergy :: Int -> Int -> AmphiState -> [(Int, AmphiState)]
  withEnergy d e s = map (\m -> (e + energyOf m, update s m)) $ moves d s

  dijkstra :: AmphiState -> Int -> Maybe (Int, AmphiState)
  dijkstra start depth = search S.empty (S.singleton (priority (0,start), (0,start)))
    where
      search :: S.Set AmphiState -> S.Set (Int, (Int, AmphiState)) -> Maybe (Int, AmphiState)
      search visited toBeVisited = case S.minView toBeVisited of
        Nothing -> Nothing
        Just ((p,(e,s)), remToVisit)
          | stop s               -> Just (e,s)
          | s `S.member` visited -> search visited remToVisit
          | otherwise            -> search visited' remToVisit'
                where
                  visited' = S.insert s visited
                  remToVisit' = foldr (\s -> S.insert (priority s, s)) remToVisit $ withEnergy depth e s

  priority :: (Int,AmphiState) -> Int
  priority (e, s) = e + valueHeuristic s

  valueHeuristic :: AmphiState -> Int
  valueHeuristic = sum . values

  values :: AmphiState -> [Int]
  values (AmphiState {..}) = map (\s-> minDistTo s 2) amber ++
                             map (\s-> 10 * (minDistTo s 4)) bronze ++
                             map (\s-> 100 * (minDistTo s 6)) copper ++
                             map (\s-> 1000 * (minDistTo s 8)) desert
      where
        minDistTo (0,c) x = abs (x-c) + 1
        minDistTo (d,c) x = if c == x then 0 else d +1 + abs (x-c)

  targetRoom :: Amphipod -> Int
  targetRoom Amber = 2
  targetRoom Bronze = 4
  targetRoom Copper = 6
  targetRoom Desert = 8

  podsInRoom :: AmphiState -> Int -> [(Amphipod, Pos)]
  podsInRoom s i = [(Amber, (r,c)) | (r,c)<-amber s, c == i] ++ [(Bronze, (r,c)) | (r,c)<-bronze s, c == i] ++
                   [(Copper, (r,c)) | (r,c)<-copper s, c == i] ++ [(Desert, (r,c)) | (r,c)<-desert s, c == i]

  assignment1 :: PuzzleType -> Int
  assignment1 _ = fst $ fromJust  $ dijkstra startState 2
    where
      startState :: AmphiState
      startState = mkState [(2,6),(2,8)] [(1,2),(1,8)] [(1,4), (1,6)] [(2,2),(2,4)]

  assignment2 :: PuzzleType -> Int
  assignment2 _ = fst $ fromJust $ dijkstra startState 4
    where
      startState :: AmphiState
      startState = mkState [(2,8), (3,6), (4,6), (4,8)] [(1,2),(1,8),(2,6),(3,4)] [(1,4), (1,6), (2,4), (3,8)] [(2,2),(3,2),(4,2), (4,4)]
      {-
      #############
      #...........#
      ###B#C#C#B###
        #D#C#B#A#
        #D#B#A#C#
        #D#D#A#A#
        #########
      -}

  instance Puzzle.Puzzle PuzzleType where
    parseInput = inputParser
    assignment1 = show . assignment1
    assignment2 = show . assignment2

  main :: IO ()
  main = Puzzle.doMain @PuzzleType

  interactive :: FilePath -> IO ()
  interactive = Puzzle.interactive @PuzzleType
